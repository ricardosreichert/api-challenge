import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import User from 'App/Models/User'

export default class AuthController {
  public async login({ auth, request, response }: HttpContextContract) {
    try {
      const { email, password } = request.only(['email', 'password'])
      const { token } = await auth.use('api').attempt(email, password)
      return { Authorization: token }
    } catch {
      return response.badRequest('Invalid credentials')
    }
  }
  public async create({ request, response }: HttpContextContract) {
    try {
      const { email, password } = request.only(['email', 'password'])

      const tool = await User.create({ email, password })

      response.status(201).json(tool.toJSON())
    } catch (error: any) {
      console.log({ error })
      response.status(500)
    }
  }
}
