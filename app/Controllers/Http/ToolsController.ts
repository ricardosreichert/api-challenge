import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Database from '@ioc:Adonis/Lucid/Database'
import Tool from 'App/Models/Tool'

export default class ToolsController {
  public async index({ response }: HttpContextContract) {
    try {
      const tool = await Tool.all()
      response.status(201).json(tool)
    } catch (error: any) {
      console.log({ error })
      response.status(404)
    }
  }

  public async create({ request, response }: HttpContextContract) {
    try {
      const { title, link, description, tags } = request.only([
        'title',
        'link',
        'description',
        'tags',
      ])

      const tool = await Tool.create({
        title,
        link,
        description,
        tags: JSON.parse(tags),
      })

      response.status(201).json(tool.toJSON())
    } catch (error: any) {
      console.log({ error })
      response.status(404)
    }
  }

  // public async store({}: HttpContextContract) {}

  public async show({ response, params }: HttpContextContract) {
    try {
      const { tag } = params
      const tool = await Database.from('tools').whereRaw(`'${tag}' = ANY(tags)`)
      response.status(200).json(tool)
    } catch (error: any) {
      response.status(404)
    }
  }

  // public async edit({}: HttpContextContract) {}

  // public async update({}: HttpContextContract) {}

  public async destroy({ response, params }: HttpContextContract) {
    try {
      const { id } = params
      const tool = await Tool.findOrFail(id)
      await tool.delete()
      response.status(204)
    } catch (error: any) {
      response.status(404)
    }
  }
}
