import { DateTime } from 'luxon'
import { BaseModel, column } from '@ioc:Adonis/Lucid/Orm'

export default class Tool extends BaseModel {
  @column({ isPrimary: true })
  public id: number

  @column()
  public title: string

  @column()
  public link: string

  @column()
  public description: string

  @column()
  public tags: string[]

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime
}
