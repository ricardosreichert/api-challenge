import Route from '@ioc:Adonis/Core/Route'

Route.get('/', async ({ view }) => {
  return view.render('home')
})

Route.post('/login', 'AuthController.login')
Route.post('/register', 'AuthController.create')

/*|-------------------|
  | Routes to Tools   |
  |-------------------|*/
Route.group(() => {
  Route.get('', 'ToolsController.index')
  Route.get(':tag', 'ToolsController.show')
  Route.post('create', 'ToolsController.create')
  Route.delete('delete/:id', 'ToolsController.destroy')
})
  .prefix('api/v1/tools')
  .middleware('auth')
