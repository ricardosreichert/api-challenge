import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class Tools extends BaseSchema {
  protected tableName = 'tools'

  public async up() {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id').primary()
      table.string('title').notNullable()
      table.string('link')
      table.string('description')
      table.specificType('tags', 'text[]')
      table.timestamp('created_at', { useTz: true })
    })
  }

  public async down() {
    this.schema.dropTable(this.tableName)
  }
}
